package com.tc.GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;

import com.tc.Impl.converter;
import com.tc.data.Data;
import com.tc.validation.validate;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.awt.event.ActionEvent;

public class convertCelsius {

	JFrame frame;
	private JTextField textField;
	private validate val = new validate();
	/**
	 * Launch the application.
	 */
	
	/**
	 * Create the application.
	 */
	public convertCelsius() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Celsius");
		frame.setBounds(100, 100, 493, 328);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblConvertFahrenheitTo = new JLabel("Convert Fahrenheit to Celsius");
		lblConvertFahrenheitTo.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblConvertFahrenheitTo.setBounds(95, 23, 291, 42);
		frame.getContentPane().add(lblConvertFahrenheitTo);
		
		JLabel lblEnterTemp = new JLabel("Enter Temperature in Fahrenheit");
		lblEnterTemp.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblEnterTemp.setBounds(67, 125, 205, 14);
		frame.getContentPane().add(lblEnterTemp);
		
		textField = new JTextField();
		textField.setBounds(314, 122, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel.setBounds(67, 171, 333, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Convert");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean flag = true;
				String Ctemp = textField.getText();
				if(val.checkEmptyInput(Ctemp)==false)
				{
					JOptionPane.showMessageDialog(null, "Input can not be Empty"); 
					flag=false;
				}
				else if(val.checkOnlyNevSign(Ctemp)==false)
				{
					JOptionPane.showMessageDialog(null, "Invalid Input"); 
					flag=false;
				}
				else if(val.checkInputValid(Ctemp)==false)
				{
					JOptionPane.showMessageDialog(null, "Invalid Input"); 
					flag=false;
				}
				else if(val.FerLowcheckValid(Ctemp)==false)
				{
					JOptionPane.showMessageDialog(null, "Value can not be less than -460 or Absolute Zero"); 
					flag=false;
				}
				else if(val.FerHighcheckValid(Ctemp)==false)
				{
					JOptionPane.showMessageDialog(null, "Value can not be more than 212"); 
					flag=false;
				}
				
				if(flag)
				{
					Double Ft = Double.parseDouble(Ctemp);
					Data temp = new Data();
					temp.setFtemp(Ft);
					converter i = new converter();
					double ss = i.convertToCelsius(temp.getFtemp());
					DecimalFormat df = new DecimalFormat("#.##");
					lblNewLabel.setText("Temperature in Celsius "+df.format(ss)+" �C");
				}
			}
		});
		btnNewButton.setBounds(201, 220, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Back");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				Navigation nev = new Navigation();
				nev.frame.setVisible(true);
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_1.setBounds(201, 254, 89, 23);
		frame.getContentPane().add(btnNewButton_1);
		
		JLabel lblNewLabel_1 = new JLabel("\u00B0F");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_1.setBounds(409, 125, 46, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblbackground = new JLabel("");
		lblbackground.setIcon(new javax.swing.ImageIcon(getClass().getResource("background.jpg")));
		lblbackground.setBounds(0, 0, 477, 289);
		frame.getContentPane().add(lblbackground);
	}
}
