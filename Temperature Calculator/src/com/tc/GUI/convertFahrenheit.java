package com.tc.GUI;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.tc.Impl.converter;
import com.tc.data.Data;
import com.tc.validation.validate;

import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.awt.event.ActionEvent;

public class convertFahrenheit {

	JFrame frame;
	private JTextField textField;
	private validate val = new validate();

	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the application.
	 */
	public convertFahrenheit() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 493, 328);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblConvertFahrenheitTo = new JLabel("Convert Celsius  to Fahrenheit");
		lblConvertFahrenheitTo.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblConvertFahrenheitTo.setBounds(95, 23, 291, 42);
		frame.getContentPane().add(lblConvertFahrenheitTo);
		
		JLabel lblEnterTemp = new JLabel("Enter Temperature in Celsius");
		lblEnterTemp.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblEnterTemp.setBounds(67, 125, 171, 14);
		frame.getContentPane().add(lblEnterTemp);
		
		textField = new JTextField();
		textField.setBounds(323, 122, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel.setBounds(67, 171, 342, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Convert");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean flag = true;
				String Ftemp = textField.getText();
				if(val.checkEmptyInput(Ftemp)==false)
				{
					JOptionPane.showMessageDialog(null, "Input can not be Empty"); 
					flag=false;
				}
				else if(val.checkOnlyNevSign(Ftemp)==false)
				{
					JOptionPane.showMessageDialog(null, "Invalid Input"); 
					flag=false;
				}
				else if(val.checkInputValid(Ftemp)==false)
				{
					JOptionPane.showMessageDialog(null, "Invalid Input"); 
					flag=false;
				}
				else if(val.CelLowcheckValid(Ftemp)==false)
				{
					JOptionPane.showMessageDialog(null, "Value can not be less than -273.15 or Absolute Zero"); 
					flag=false;
				}
				else if(val.CelHighcheckValid(Ftemp)==false)
				{
					JOptionPane.showMessageDialog(null, "Value can not be more than 100"); 
					flag=false;
				}
				
				if(flag)
				{
					Double Ft = Double.parseDouble(Ftemp);
					Data temp =new Data();
					temp.setCtemp(Ft);
					converter i = new converter();
					double ss = i.convertToFahrenheit(temp.getCtemp());
					DecimalFormat df = new DecimalFormat("#.##");
					lblNewLabel.setText("Temperature in Fahrenheit : "+df.format(ss)+" �F");
				}
			}
		});
		btnNewButton.setBounds(188, 221, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Back");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				Navigation nev = new Navigation();
				nev.frame.setVisible(true);
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_1.setBounds(188, 255, 89, 23);
		frame.getContentPane().add(btnNewButton_1);
		
		JLabel lblNewLabel_1 = new JLabel("\u00B0C");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_1.setBounds(419, 125, 29, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblbackground = new JLabel("");
		lblbackground.setIcon(new javax.swing.ImageIcon(getClass().getResource("background.jpg")));
		lblbackground.setBounds(0, 0, 477, 289);
		frame.getContentPane().add(lblbackground);
	}
}
