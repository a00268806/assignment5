package com.tc.GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Navigation {

	 JFrame frame;
	private final JButton btnNewButton = new JButton("Convert To Celsius");

	/**
	 * Launch the application.
	 */


	/**
	 * Create the application.
	 */
	public Navigation() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 493, 328);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				convertCelsius cc = new convertCelsius();
				cc.frame.setVisible(true);
			}
		});
		btnNewButton.setBounds(161, 82, 169, 31);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Convert To Fahrenheit");
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				convertFahrenheit cf = new convertFahrenheit();
				cf.frame.setVisible(true);
			}
		});
		btnNewButton_1.setBounds(161, 188, 169, 31);
		frame.getContentPane().add(btnNewButton_1);
		
		JLabel lblNewLabel = new JLabel("Temperature Conversion");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblNewLabel.setBounds(125, 25, 248, 31);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblbackground = new JLabel("");
		lblbackground.setIcon(new javax.swing.ImageIcon(getClass().getResource("background.jpg")));
		lblbackground.setBounds(0, 0, 477, 289);
		frame.getContentPane().add(lblbackground);
	}
}
