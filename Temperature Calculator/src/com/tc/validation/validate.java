package com.tc.validation;

import java.util.regex.Pattern;

public class validate {

	public boolean checkInputValid(String str) {
	 	
		if (str.matches("[-0-9.]+")) {
			return true;
		}
		return false;
	}
	public boolean checkOnlyNevSign(String str) {
		
		if (str.equals("-")||str.equals(".")) {
			return false;
		}
		return true;
	}
	public boolean checkEmptyInput(String str)
	{
		if (str.equals("") )
		{
			return false;
		}
		return true;
	}
	
	public boolean CelLowcheckValid(String str)
	{
		
		Double temp = Double.parseDouble(str);
		if(temp < -273.15)
		{
			return false;
		}
		return true;
	}
	public boolean CelHighcheckValid(String str)	
	{
		Double temp = Double.parseDouble(str);
		if(temp > 100)
		{
			return false;
		}
		return true;
	}

	public boolean FerLowcheckValid(String str)
	{
		Double temp = Double.parseDouble(str);
		if(temp < -460)
		{
			return false;
		}
		return true;
		
		
	}
	public boolean FerHighcheckValid(String str)
	{
		Double temp = Double.parseDouble(str);
		if(temp > 212)
		{
			return false;
		}
		return true;
		
		
	}
}
