package com.tc.Impl;

import com.tc.Dao.temCalcualtorDao;
import com.tc.data.Data;

public class converter implements temCalcualtorDao {

	@Override
	public double convertToFahrenheit(double temp) {
		 double F = temp * 1.8 + 32;
		 
		return F;
	}

	@Override
	public double convertToCelsius(double temp) {
		 double c = (temp - 32 ) *  5/9;
		 
		return c;
	}
}
