package com.tc.Junit;


import com.tc.Impl.converter;

import junit.framework.Assert;
import junit.framework.TestCase;

public class testImpl extends TestCase {
	converter cc = new converter();

	// Test case id:1
	// Test case objective: To check convertToFahrenheit function
	// Test case input:8
	// Test case Excepted output:
	String output = "46.4";
	public void testInput001() {
		double ans = cc.convertToFahrenheit(8);
		String convert = ""+ans;
		Assert.assertEquals(output, convert);
	}

	// Test case id:2
	// Test case objective: To check convertToCelsius function
	// Test case input:8
	// Test case Excepted output:
	String output2 = "-13.333333333333334";
	public void testInput002() {
		double ans = cc.convertToCelsius(8);
		String convert = ""+ans;
		Assert.assertEquals(output2, convert);
	}
}
